// Title component for a portfolio table.
// Provides controls to delete or rename the portfolio.

import React from "react";
import { connect } from "react-redux";

import IconButton from "@material-ui/core/IconButton";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Tooltip from "@material-ui/core/Tooltip";

import CheckButton from "@material-ui/icons/Check";
import DeleteButton from "@material-ui/icons/Delete";
import EditButton from "@material-ui/icons/Edit";

import {
    deletePortfolio,
    renamePortfolio,
} from "../../store/actions";
import {
    State,
} from "../../store/state";

import DeleteDialog from "./delete";

// Props passed in by the caller.
interface PublicProps {
    id: string;
}

interface StateProps extends PublicProps {
    name: string;
    title: string;
}

interface Props extends StateProps {
    deletePortfolio: typeof deletePortfolio;
    renamePortfolio: typeof renamePortfolio;
}

const useStyles = makeStyles((theme) => ({
    button: {
      margin: theme.spacing(1),
    },
    input: {
      display: "none",
    },
    textField: {
      marginTop: theme.spacing(2),
    },
}));

function Title(props: Props) {
    const classes = useStyles();
    const [editMode, setEditMode] = React.useState(false);
    const [name, setName] = React.useState(props.name);
    const [dialogOpen, setDialogOpen] = React.useState(false);

    const openDeleteDialog = async () => {
        setDialogOpen(true);
    };

    const closeDeleteDialog = () => {
        setDialogOpen(false);
    };

    const deleteSelf = () => {
        closeDeleteDialog();
        props.deletePortfolio(
            props.id,
        );
    };

    return <div>
        {editMode ? <span>
            <TextField
                className={classes.textField}
                onChange={(event) => {
                    setName(event.target.value);
                }}
                defaultValue={props.name}
            />
            <IconButton
                className={classes.button}
                onClick={() => {
                    setEditMode(false);
                    props.renamePortfolio(props.id, name);
                }}
                title="OK"
            >
                <CheckButton/>
            </IconButton>
        </span> : <span>
            {props.title}
            <Tooltip title="Rename portfolio">
                <IconButton
                    className={classes.button}
                    onClick={() => setEditMode(true)}
                >
                    <EditButton/>
                </IconButton>
            </Tooltip>
        </span>}
        <Tooltip title="Delete portfolio">
            <IconButton
                className={classes.button}
                onClick={openDeleteDialog}
            >
                <DeleteButton/>
            </IconButton>
        </Tooltip>
        <DeleteDialog
            name={props.name}
            open={dialogOpen}
            onClose={closeDeleteDialog}
            onDelete={deleteSelf}
        />
    </div>;
}

function mapStateToProps(state: State, ownProps: PublicProps): StateProps {
    const portfolio = state.portfolios[ownProps.id];
    const value = portfolio.totalValue.toLocaleString(
        "en-US", {currency: "USD", style: "currency"},
    );
    return {
        id: ownProps.id,
        name: portfolio.name,
        title: `${portfolio.name} (${value})`,
    };
}

export default connect(
    mapStateToProps,
    { deletePortfolio, renamePortfolio },
)(Title);
