// Select widget for market assets.
import React from "react";

import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";

interface OwnProps {
    symbols: string[];
    onChange: (value: string|unknown) => void;
    value: string;
}

interface Props extends OwnProps {}

export default function SelectAsset(props: Props) {
    if (!props.symbols.length) {
        return null;
    }
    let value = props.value;
    if (!value) {
        value = props.symbols[0];
    }
    return <Select
        data-testid="portfolio-select"
        onChange={(e) => props.onChange(e.target.value)}
        value={value}
    >
        {props.symbols.map((s) =>
            <MenuItem value={s} key={s}>{s}</MenuItem>,
        )}
    </Select>;
}
