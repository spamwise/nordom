import React from "react";

import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";

interface Props {
    name: string;
    open: boolean;
    onClose: () => void;
    onDelete: () => void;
}

export default function DeleteDialog(props: Props) {
    return <Dialog
        data-testid="delete-dialog"
        open={props.open}
        onClose={props.onClose}
    >
        <DialogContent><DialogContentText>
            Delete {props.name}?
        </DialogContentText></DialogContent>
        <DialogActions>
            <Button onClick={props.onClose} color="primary">
                Cancel
            </Button>
            <Button onClick={props.onDelete} color="primary" autoFocus>
                Delete
            </Button>
        </DialogActions>
    </Dialog>;
}
