import React from "react";
import { connect } from "react-redux";

import MaterialTable, { MTableBodyRow, MTableEditRow } from "material-table";

import Paper from "@material-ui/core/Paper";
import {
    addHolding,
    deleteHolding,
    updateHolding,
} from "../../store/actions";
import { State, theDollar } from "../../store/state";
import Select from "./select";
import Title from "./title";

interface Row {
    symbol: string;
    quantity: string;
    value: string;
    targetPct: string;
    actualPct: string;
    delta: number;
    align: string;
    targetPctNum: number;
}

interface OwnProps {
    id: string;
}

interface StateProps extends OwnProps {
    canAdd: boolean;
    freeAssets: string[];
    name: string;
    rows: Row[];
}

interface Props extends StateProps {
    addHolding: typeof addHolding;
    deleteHolding: typeof deleteHolding;
    updateHolding: typeof updateHolding;
}

function Table(props: Props) {

    const addRow = async (row: Row) => {
        props.addHolding(
            props.id,
            row.symbol,
            row.quantity,
            row.targetPct,
        );
    };

    const deleteRow = async (row: Row) => {
        props.deleteHolding(
            props.id,
            row.symbol,
        );
    };

    const updateRow = async (row: Row) => {
        props.updateHolding(
            props.id,
            row.symbol,
            row.quantity,
            row.targetPct,
        );
    };

    const targetPctStyle = (data: Row[], row?: Row): React.CSSProperties => {
        const pctSum = props.rows.reduce(
            (prev: Row, curr: Row) => {
                return {...prev, targetPctNum: prev.targetPctNum + curr.targetPctNum};
            }
        ).targetPctNum;
        if (pctSum < 100.0) {
            return { backgroundColor: "lightgoldenrodyellow" };
        }
        if (pctSum > 100.0) {
            return { backgroundColor: "peachpuff" };
        }
        return {};
    }

    const tradeCellStyle = (data: Row[], row?: Row): React.CSSProperties => {
        if (!row) {
            return {};
        }
        if (row.delta < 0 ) {
            return { color: "blue" };
        }
        if (row.delta > 0 ) {
            return { color: "green" };
        }
        return {};
    };

    return <MaterialTable
        data-testid={"portfolio-" + props.name}
        title={<Title id={props.id}/>}
        columns={[
            {title: "Symbol", field: "symbol", editable: "onAdd", editComponent:
                (p) => (<Select
                    symbols={props.freeAssets}
                    onChange={p.onChange}
                    value={p.value}
                />),
            },
            {title: "Quantity", field: "quantity", type: "numeric"},
            {title: "Value", field: "value", editable: "never"},
            {title: "Target %", field: "targetPct", type: "numeric", defaultSort: "desc",
             cellStyle: targetPctStyle },
            {title: "Current %", field: "actualPct", type: "numeric", editable: "never"},
            {title: "Trade", field: "align", editable: "never",
             cellStyle: tradeCellStyle },
        ]}
        components={{
            Container: (p) => (
                <Paper
                    data-testid={"portfolio-" + props.name}
                    {...p}
                />
            ),
            EditRow: (p) => (
                <MTableEditRow
                    data-testid={"portfolio-edit-row"}
                    {...p}
                />
            ),
            Row: (p) => (
                <MTableBodyRow
                    data-testid={"portfolio-row-" + p.data.symbol}
                    {...p}
                />
            ),
        }}
        data={props.rows}
        editable={{
            onRowAdd: props.canAdd ? addRow : undefined,
            onRowDelete: deleteRow,
            onRowUpdate: updateRow,
        }}
        localization={{
            body: {
                addTooltip: "Add Asset",
                emptyDataSourceMessage: "No assets in portfolio.",
            },
        }}
        options={{
            paging: false,
            search: false,
        }}
        style={{
            marginBottom: 10,
            marginTop: 10,
        }}
    />;
}

function mapStateToProps(state: State, ownProps: OwnProps): StateProps {
    const id = ownProps.id;
    const p = state.portfolios[id];
    const name = p.name;
    const rows: Row[] = [];
    for (const symbol in p.holdings) {
        const h = p.holdings[symbol];
        const delta = h.targetQty - h.quantity;
        let align = symbol === theDollar ?
            delta.toLocaleString("en-US", {currency: "USD", style: "currency"}) :
            delta.toLocaleString();
        if (delta > 0) {
            const verb = symbol === theDollar ? "realize" : "buy";
            align = "+" + align + " (" + verb + ")";
        } else if (delta < 0) {
            const verb = symbol === theDollar ? "spend " : "sell";
            align = align + " (" + verb + ")";
        } else {
            align = "";
        }
        rows.push({
            symbol,
            quantity: h.quantity.toFixed(2),
            targetPct: h.targetPct.toLocaleString(),
            actualPct: h.actualPct.toLocaleString(),
            value: h.value.toLocaleString("en-US", {currency: "USD", style: "currency"}),
            delta,
            align,
            targetPctNum: h.targetPct,
        });
    }
    const freeAssets = Object.keys(state.market.assets)
        .filter((symbol: string) => {
            return !(symbol in p.holdings);
        });
    const canAdd = freeAssets.length > 0;
    return {
        canAdd,
        freeAssets,
        id,
        name,
        rows,
    };
}

export default connect(
    mapStateToProps,
    {addHolding, deleteHolding, updateHolding},
)(Table);
