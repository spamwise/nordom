import React from "react";
import { connect } from "react-redux";

import Container from "@material-ui/core/Container";

import {
    State,
} from "../store/state";

import Table from "./portfolio/table";

interface Props {
    portfolioIds: string[];
}

function Portfolios(props: Props) {
    return <Container
        data-testid="portfolios"
    >
        {props.portfolioIds.map((id) =>
            <Table
                id={id}
                key={id}
            />,
        )}
    </Container>;
}

function mapStateToProps(state: State): Props {
    const portfolioIds = Object.keys(state.portfolios);
    return {
        portfolioIds,
    };
}

export default connect(
    mapStateToProps,
)(Portfolios);
