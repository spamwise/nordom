import React, { Component } from "react";

import Container from "@material-ui/core/Container";
import MaterialTable, { MTableBodyRow, MTableEditRow } from "material-table";

import { connect } from "react-redux";
import { addAsset, deleteAsset, refreshAssets, updateAsset } from "../store/actions";
import { State, theDollar } from "../store/state";

import humanizeDuration from "humanize-duration";

interface Row {
    symbol: string;
    price: number;
    updated: string;
    updatedDelta: number;
}

interface Props {
    // state
    assets: Row[];
    canRefreshAssets: boolean;

    // actions
    addAsset: typeof addAsset;
    deleteAsset: typeof deleteAsset;
    refreshAssets: typeof refreshAssets;
    updateAsset: typeof updateAsset;
}

class Market extends Component<Props> {
    // Actual view for the market, rendering market assets and associated inputs.
    public addRow = async (rowData: Row) => {
        this.props.addAsset(rowData.symbol, rowData.price);
        return;
    }

    public deleteRow = async (rowData: Row) => {
        this.props.deleteAsset(rowData.symbol);
        return;
    }

    public updateRow = async (rowData: Row) => {
        this.props.updateAsset(rowData.symbol, rowData.price);
        return;
    }

    public updatedStyle = (data: Row[], row?: Row): React.CSSProperties => {
        if (!row) {
            return {};
        }
        if (row.updatedDelta > 1000 * 60 * 60 * 24 * 7 ) {
            return { color: "red" };
        }
        if (row.updatedDelta > 1000 * 60 * 60 * 24 ) {
            return { color: "orangered" };
        }
        if (row.updatedDelta > 1000 * 60 * 60 ) {
            return { color: "darkgoldenrod" };
        }
        if (row.updatedDelta > 1000 * 60 * 5 ) {
            return { color: "olive" };
        }
        return { color: "darkgreen" };
    }

    public render() {
        return <Container>
            <MaterialTable
                title="Market Data"
                actions={[
                    {
                      disabled: !this.props.canRefreshAssets,
                      icon: "refresh",
                      isFreeAction: true,
                      tooltip: this.props.canRefreshAssets ?
                               "Load Asset Prices" :
                               "Must set API key to auto-load asset prices",
                      onClick: (event) => {
                        this.props.refreshAssets();
                      },
                    },
                ]}
                columns={[
                    {title: "Symbol", field: "symbol", editable: "onAdd"},
                    {title: "Price", field: "price", type: "currency", currencySetting: {
                        currencyCode: "USD", maximumFractionDigits: 2,
                    }},
                    {title: "Last updated", field: "updated", editable: "never",
                    cellStyle: this.updatedStyle},
                ]}
                components={{
                    Row: (props) => (
                        <MTableBodyRow
                            data-testid={"market-row-" + props.data.symbol}
                            {...props}
                        />
                    ),
                    EditRow: (props) => (
                        <MTableEditRow
                            data-testid={"market-edit-row"}
                            {...props}
                        />
                    ),
                }}
                data={this.props.assets}
                editable={{
                    isEditable: (rowData) => rowData.symbol !== theDollar,
                    isDeletable: (rowData) => rowData.symbol !== theDollar,
                    onRowAdd: this.addRow,
                    onRowDelete: this.deleteRow,
                    onRowUpdate: this.updateRow,
                }}
                localization={{
                    body: {
                        addTooltip: "Add Asset",
                        emptyDataSourceMessage: "Add assets whose prices you want to track.",
                    },
                }}
                options={{
                    paging: false,
                    search: false,
                }}
                style={{
                    marginBottom: 20,
                    marginTop: 10,
                }}
            />
        </Container>;
    }
}

// Hook it up to the store.

function mapStateToProps(state: State) {
    // Viewmodel for the market, translating between the state and the view.
    const assets = state.market.assets;
    return {
        assets: Object.keys(assets)
            .filter((symbol: string) => symbol !== theDollar)
            .map((symbol: string) => {
                const updatedTime = assets[symbol].updated;
                const nowTime = Date.now();
                const updatedDelta = nowTime - updatedTime;
                const updated = updatedTime ?
                    humanizeDuration(updatedDelta, {
                        largest: 1,
                        round: true,
                    }) : "manual";
                return {
                    symbol,
                    price: assets[symbol].price,
                    updated,
                    updatedDelta,
                };
            }),
        canRefreshAssets: Boolean(state.market.apiKey),
    };
}

export default connect(
    mapStateToProps,
    { addAsset, deleteAsset, refreshAssets, updateAsset },
)(Market);
