import clsx from "clsx";
import React, { Component } from "react";

import IconButton from "@material-ui/core/IconButton";
import Snackbar from "@material-ui/core/Snackbar";
import SnackbarContent from "@material-ui/core/SnackbarContent";

import CloseIcon from "@material-ui/icons/Close";
import ErrorIcon from "@material-ui/icons/Error";
import InfoIcon from "@material-ui/icons/Info";

import { makeStyles } from "@material-ui/core/styles";

import { connect } from "react-redux";
import { hideMessage } from "../store/actions";
import { State, UserMessage } from "../store/state";

const variantIcon = {
    error: ErrorIcon,
    info: InfoIcon,
};

const useStyles = makeStyles((theme) => ({
    error: {
      backgroundColor: theme.palette.error.dark,
    },
    info: {
      backgroundColor: theme.palette.primary.main,
    },
    icon: {
      fontSize: 20,
    },
    iconVariant: {
      opacity: 0.9,
      marginRight: theme.spacing(1),
    },
    message: {
      display: "flex",
      alignItems: "center",
    },
}));

interface ContentProps {
    message: string;
    type: "info" | "error";
    onClose: () => void;
}

function MessageContent(props: ContentProps) {
    const classes = useStyles();
    const Icon = variantIcon[props.type];

    return <SnackbarContent
        className={classes[props.type]}
        key={props.message}
        message={
            <span id="client-snackbar" className={classes.message}>
                <Icon className={clsx(classes.icon, classes.iconVariant)} />
                {props.message}
            </span>
            }
        action={[
            <IconButton key="close" aria-label="close" color="inherit" onClick={props.onClose}>
                <CloseIcon className={classes.icon} />
            </IconButton>,
            ]}
    />;
}

interface MessageProps {
    // state
    userMessage: UserMessage;

    // actions
    hideMessage: typeof hideMessage;
}

class MessageBar extends Component<MessageProps> {
    public render() {
        return (
            <Snackbar
                data-testid="message-snackbar"
                key={this.props.userMessage.msg}
                open={this.props.userMessage.visible}
            >
                <MessageContent
                    message={this.props.userMessage.msg}
                    type={this.props.userMessage.type}
                    onClose={this.props.hideMessage}
                />
            </Snackbar>
        );
    }
}

function mapStateToProps(state: State) {
    // Viewmodel for the message, translating between the state and the view.
    return {
        userMessage: state.userMessage,
    };
}

export default connect(
    mapStateToProps,
    { hideMessage },
)(MessageBar);
