// Toolbar at the bottom of the app for infrequent top-level actions.
import React from "react";
import { connect } from "react-redux";

import Button from "@material-ui/core/Button";
import Container from "@material-ui/core/Container";
import { makeStyles } from "@material-ui/core/styles";

import { addPortfolio, saveState } from "../store/actions";
import ApiButton from "./apiButton";

interface Props {
    addPortfolio: typeof addPortfolio;
    saveState: typeof saveState;
}

const useStyles = makeStyles((theme) => ({
    button: {
      margin: theme.spacing(1),
    },
}));

function Toolbar(props: Props) {
    const classes = useStyles();

    return <Container><div>
        <Button
            className={classes.button}
            onClick={props.addPortfolio}
            variant="contained"
        >
            New Portfolio
        </Button>
        <Button
            className={classes.button}
            onClick={props.saveState}
            variant="contained"
        >
            Save Data
        </Button>
        <ApiButton/>
    </div></Container>;
}

export default connect(
    null,
    { addPortfolio, saveState },
)(Toolbar);
