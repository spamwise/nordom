import React from "react";
import { connect } from "react-redux";

import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import TextField from "@material-ui/core/TextField";

import { setApiKey } from "../store/actions";
import { State } from "../store/state";

interface Props {
    apiKey: string;
    setApiKey: typeof setApiKey;
}

function ApiButton(props: Props) {
    const [open, setOpen] = React.useState(false);
    const [key, setKey] = React.useState(props.apiKey);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleSetKey = () => {
        handleClose();
        props.setApiKey(key);
    };

    return (
      <span data-testid="api-button">
        <Button
            variant="contained"
            color={props.apiKey ? undefined : "primary"}
            onClick={handleClickOpen}
        >
          Set API Key
        </Button>
        <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
          <DialogTitle id="form-dialog-title">Enter AlphaVantage API Key</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Enter your <a
                href="https://www.alphavantage.co/support/#api-key"
                rel="noopener noreferrer"
                target="_blank"
              >
                AlphaVantage API key
              </a> to load market data automatically.
            </DialogContentText>
            <TextField
              autoFocus
              data-testid="api-field"
              defaultValue={props.apiKey}
              margin="dense"
              id="api_key"
              onChange={(event) => {
                  setKey(event.target.value);
                }}
              fullWidth
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={handleSetKey} color="primary">
              Update Key
            </Button>
          </DialogActions>
        </Dialog>
      </span>
    );
}

function mapStateToProps(state: State): {apiKey: string} {
    return {
        apiKey: state.market.apiKey,
    };
}

export default connect(
    mapStateToProps,
    { setApiKey },
)(ApiButton);
