// All the logic about what states are valid
// and how actions affect state lives here.

import { Action, Actions } from "./actions";
import {
    initialState,
    State,
    theDollar,
} from "./state";

function addAsset(symbol: string, price: number, state: State): State {
    // Add a new asset with the given symbol.
    if (symbol in state.market.assets) {
        return showError("Asset already exists!", state);
    }
    if (!symbol) {
        return showError("Must specify a symbol!", state);
    }
    return {
        ...state,
        market: {
            ...state.market,
            assets: {
                ...state.market.assets,
                [symbol]: {
                    price,
                    updated: 0,
                },
            },
        },
    };
}

function addHolding(
    id: string,
    symbol: string,
    quantity: number,
    targetPct: number,
    state: State,
): State {
    if (!(symbol in state.market.assets)) {
        return showError("No such symbol!", state);
    }
    const portfolio = state.portfolios[id];
    if (!portfolio || symbol in portfolio.holdings) {
        return showError("Already part of this portfolio!", state);
    }
    return {
        ...state,
        portfolios: {
            ...state.portfolios,
            [id]: {
                ...state.portfolios[id],
                holdings: {
                    ...state.portfolios[id].holdings,
                    [symbol]: {
                        quantity,
                        targetPct,
                        value: 0,
                        targetQty: 0,
                        actualPct: 0,
                    },
                },
            },
        },
    };
}

function addPortfolio(state: State): State {
    // Add a new (empty) portfolio.
    let pid = 0;
    for (const id in state.portfolios) {
        pid = Math.max(pid, +id);
    }
    pid++;
    return {
        ...state,
        portfolios: {
            ...state.portfolios,
            [pid]: {
                totalValue: 0.0,
                name: "Portfolio " + pid,
                holdings: {
                    [theDollar]: {
                        quantity: 0.0,
                        targetPct: 100.0,
                        targetQty: 0.0,
                        actualPct: 0.0,
                        value: 0.0,
                    },
                },
            },
        },
    };
}

function deleteAsset(symbol: string, state: State): State {
    if (!(symbol in state.market.assets)) {
        return showError("no such asset", state);
    }
    if (symbol === theDollar) {
        return showError("You can't delete the almighty buck.", state);
    }
    for (const [, p] of Object.entries(state.portfolios)) {
        if (symbol in p.holdings) {
            return showError(
                `Can't delete ${symbol} because it's included in ${p.name}.`,
                state,
            );
        }
    }
    const {
        [symbol]: _,
        ...assets
    } = state.market.assets;
    return {
        ...state,
        market: {
            ...state.market,
            assets,
        },
    };
}

function deleteHolding(id: string, symbol: string, state: State): State {
    if (!(id in state.portfolios)) {
        return showError("no such portfolio", state);
    }
    if (!(symbol in state.portfolios[id].holdings)) {
        return showError("no such asset in this portfolio", state);
    }
    const {
        [symbol]: _,
        ...holdings
    } = state.portfolios[id].holdings;
    return {
        ...state,
        portfolios: {
            ...state.portfolios,
            [id]: {
                ...state.portfolios[id],
                holdings,
            },
        },
    };
}

function deletePortfolio(id: string, state: State): State {
    if (!(id in state.portfolios)) {
        return showError("no such portfolio", state);
    }
    const {
        [id]: _,
        ...portfolios
    } = state.portfolios;
    return {
        ...state,
        portfolios,
    };
}

function hideMessage(state: State): State {
    return {
        ...state,
        userMessage: {
            ...state.userMessage,
            visible: false,
        },
    };
}

function renamePortfolio(id: string, name: string, state: State): State {
    if (!(id in state.portfolios)) {
        return showError("No such portfolio!", state);
    }
    return {
        ...state,
        portfolios: {
            ...state.portfolios,
            [id]: {
                ...state.portfolios[id],
                name,
            },
        },
    };
}

function setApiKey(
    apiKey: string,
    state: State,
): State {
    return {
        ...state,
        market: {
            ...state.market,
            apiKey,
        },
    };
}

function updateAsset(
    symbol: string,
    price: number,
    state: State,
): State {
    return updateAssetWithTime(symbol, price, 0, state);
}

function updateAssetWithTime(
    symbol: string,
    price: number,
    updated: number,
    state: State,
): State {
    if (!(symbol in state.market.assets)) {
        return showError("No such symbol!", state);
    }
    if (symbol === theDollar) {
        return showError("Currency manipulation not allowed.", state);
    }

    return {
        ...state,
        market: {
            ...state.market,
            assets: {
                ...state.market.assets,
                [symbol]: {price, updated},
            },
        },
    };
}

function showError(msg: string, state: State): State {
    return {
        ...state,
        userMessage: {
            msg,
            type: "error",
            visible: true,
        },
    };
}

/*
function showInfo(msg: string, state: State): State {
    return {
        ...state,
        userMessage: {
            msg,
            type: "info",
            visible: true,
        }
    }
}
*/

function updateHolding(
    id: string,
    symbol: string,
    quantity: number,
    targetPct: number,
    state: State,
): State {
    if (!(id in state.portfolios)) {
        return showError("No such portfolio!", state);
    }
    if (!(symbol in state.portfolios[id].holdings)) {
        return showError("No such asset in this portfolio!", state);
    }
    return {
        ...state,
        portfolios: {
            ...state.portfolios,
            [id]: {
                ...state.portfolios[id],
                holdings: {
                    ...state.portfolios[id].holdings,
                    [symbol]: {
                        ...state.portfolios[id].holdings[symbol],
                        quantity,
                        targetPct,
                    },
                },
            },
        },
    };
}

function updatePortfolios(state: State): State {
    // Rebuild portfolios with updated values/percentages.
    const assets = state.market.assets;
    const portfolios: typeof state.portfolios = {};

    for (const id in state.portfolios) {
        const p = state.portfolios[id];

        let totalValue = 0.0;
        const values: {[symbol: string]: number} = {};
        for (const symbol in p.holdings) {
            const h = p.holdings[symbol];
            values[symbol] = h.quantity * assets[symbol].price;
            totalValue += values[symbol];
        }
        portfolios[id] = {
            ...p,
            holdings: {},
            totalValue,
        };

        for (const symbol in p.holdings) {
            const h = p.holdings[symbol];
            const value = values[symbol];
            const targetVal = totalValue * h.targetPct / 100.0;
            portfolios[id].holdings[symbol] = {
                ...h,
                value,
                targetQty: targetVal / assets[symbol].price,
                actualPct: 100 * value / totalValue,
            };
        }
    }

    return {
        ...state,
        portfolios,
    };
}

export default function reduce(
    state = initialState,
    action: Action,
): State {
    // The master reducer.
    switch (action.type) {
        case Actions.ADD_ASSET:
            return addAsset(
                action.symbol,
                action.price,
                state,
            );
        case Actions.ADD_PORTFOLIO:
            return addPortfolio(
                state,
            );
        case Actions.ADD_HOLDING:
            return addHolding(
                action.id,
                action.symbol,
                action.quantity,
                action.targetPct,
                state,
            );
        case Actions.DELETE_ASSET:
            return deleteAsset(
                action.symbol,
                state,
            );
        case Actions.DELETE_HOLDING:
            return deleteHolding(
                action.id,
                action.symbol,
                state,
            );
        case Actions.DELETE_PORTFOLIO:
            return deletePortfolio(
                action.id,
                state,
            );
        case Actions.HIDE_MESSAGE:
            return hideMessage(
                state,
            );
        case Actions.RENAME_PORTFOLIO:
            return renamePortfolio(
                action.id,
                action.name,
                state,
            );
        case Actions.SET_API_KEY:
            return setApiKey(
                action.apiKey,
                state,
            );
        case Actions.SHOW_ERROR:
            return showError(
                action.message,
                state,
            );
        case Actions.UPDATE_ASSET:
            return updateAsset(
                action.symbol,
                action.price,
                state,
            );
        case Actions.UPDATE_ASSET_WITH_TIME:
            return updateAssetWithTime(
                action.symbol,
                action.price,
                action.updated,
                state,
            );
        case Actions.UPDATE_HOLDING:
            return updateHolding(
                action.id,
                action.symbol,
                action.quantity,
                action.targetPct,
                state,
            );
        case Actions.UPDATE_PORTFOLIOS:
            return updatePortfolios(
                state,
            );
        default:
            return state;
    }
}
