import { SagaIterator } from "redux-saga";
import { all, call, fork, put, select, takeLatest } from "redux-saga/effects";

import { Actions, showError, updatePortfolios } from "./actions";
import { fetchPrice } from "./server";
import { saveState, State, theDollar } from "./state";

function sleep(ms: number) {
    return new Promise(res => {
        setTimeout(res, ms);
    })
}

function *refreshAssetsSaga(): SagaIterator {
    // Trigger a refresh of all assets.
    const updateAsset = function*(symbol: string) {
        const apiKey = yield select((s: State) => s.market.apiKey);
        let delay = 60;
        while (true) {
            try {
                const price = yield call(fetchPrice, symbol, apiKey);
                yield put({
                    type: Actions.UPDATE_ASSET_WITH_TIME,
                    symbol,
                    price,
                    updated: Date.now(),
                });
                break;
            } catch (e) {
                if(!(e as Error).message.includes("API call frequency") 
                    || delay > 300 ) {
                    throw e;
                }
                yield sleep(delay * 1000);
                delay *= 1.5;
            }
        }
    };

    yield takeLatest([
        Actions.REFRESH_ASSETS,
    ], function*() {
        const symbols = yield select((s: State) => {
            return Object.keys(s.market.assets)
                .filter((symbol: string) => symbol !== theDollar);
        });
        try {
            yield all(symbols.map(
                (symbol: string) => {
                    return updateAsset(symbol);
                },
            ));
        } catch (err) {
            yield put(showError((err as Error).message));
        }
    });
}

function* saveStateSaga(): SagaIterator {
    // Save the state out on the SAVE_STATE action.
    yield takeLatest([
        Actions.SAVE_STATE,
    ], function*() {
        const state = yield select((s: State) => s);
        yield call(saveState, state);
    });
}

function* updatePortfoliosSaga(): SagaIterator {
    // Recalculate portfolio balance every time
    // an asset price or holding is updated.
    yield takeLatest([
        Actions.ADD_HOLDING,
        Actions.DELETE_HOLDING,
        Actions.UPDATE_ASSET,
        Actions.UPDATE_ASSET_WITH_TIME,
        Actions.UPDATE_HOLDING,
    ], function*() {
        yield put(updatePortfolios());
    });
}

export default function* rootSaga(): SagaIterator {
    yield all([
        fork(refreshAssetsSaga),
        fork(saveStateSaga),
        fork(updatePortfoliosSaga),
    ]);
}
