// Actions and their creators.

export enum Actions {
    ADD_ASSET = "ADD_ASSET",
    ADD_HOLDING = "ADD_HOLDING",
    ADD_PORTFOLIO = "ADD_PORTFOLIO",
    DELETE_ASSET = "DELETE_ASSET",
    DELETE_HOLDING = "DELETE_HOLDING",
    DELETE_PORTFOLIO = "DELETE_PORTFOLIO",
    HIDE_MESSAGE = "HIDE_MESSAGE",
    REFRESH_ASSETS = "REFRESH_ASSETS",
    RENAME_PORTFOLIO = "RENAME_PORTFOLIO",
    SAVE_STATE = "SAVE_STATE",
    SET_API_KEY = "SET_API_KEY",
    SHOW_ERROR = "SHOW_ERROR",
    UPDATE_ASSET = "UPDATE_ASSET",
    UPDATE_ASSET_WITH_TIME = "UPDATE_ASSET_WITH_TIME",
    UPDATE_HOLDING = "UPDATE_HOLDING",
    UPDATE_PORTFOLIOS = "UPDATE_PORTFOLIOS",
}

interface AddAssetAction {
    type: typeof Actions.ADD_ASSET;
    symbol: string;
    price: number;
}

interface AddHoldingAction {
    type: typeof Actions.ADD_HOLDING;
    id: string;
    symbol: string;
    quantity: number;
    targetPct: number;
}

interface AddPortfolioAction {
    type: typeof Actions.ADD_PORTFOLIO;
}

interface DeleteAssetAction {
    type: typeof Actions.DELETE_ASSET;
    symbol: string;
}

interface DeleteHoldingAction {
    type: typeof Actions.DELETE_HOLDING;
    id: string;
    symbol: string;
}

interface DeletePortfolioAction {
    type: typeof Actions.DELETE_PORTFOLIO;
    id: string;
}

interface HideMessageAction {
    type: typeof Actions.HIDE_MESSAGE;
}

interface RefreshAssetsAction {
    type: typeof Actions.REFRESH_ASSETS;
}

interface RenamePortfolioAction {
    type: typeof Actions.RENAME_PORTFOLIO;
    id: string;
    name: string;
}

interface SaveStateAction {
    type: typeof Actions.SAVE_STATE;
}

interface SetApiKeyAction {
    type: typeof Actions.SET_API_KEY;
    apiKey: string;
}

interface ShowErrorAction {
    type: typeof Actions.SHOW_ERROR;
    message: string;
}

interface UpdateAssetAction {
    type: typeof Actions.UPDATE_ASSET;
    symbol: string;
    price: number;
}

interface UpdateAssetWithTimeAction {
    type: typeof Actions.UPDATE_ASSET_WITH_TIME;
    symbol: string;
    price: number;
    updated: number;
}

interface UpdateHoldingAction {
    type: typeof Actions.UPDATE_HOLDING;
    id: string;
    symbol: string;
    quantity: number;
    targetPct: number;
}

interface UpdatePortfoliosAction {
    type: typeof Actions.UPDATE_PORTFOLIOS;
}

export type Action = (
    AddAssetAction |
    AddHoldingAction |
    AddPortfolioAction |
    DeleteAssetAction |
    DeleteHoldingAction |
    DeletePortfolioAction |
    HideMessageAction |
    RefreshAssetsAction |
    RenamePortfolioAction |
    SaveStateAction |
    SetApiKeyAction |
    ShowErrorAction |
    UpdateAssetAction |
    UpdateAssetWithTimeAction |
    UpdateHoldingAction |
    UpdatePortfoliosAction
);

export function addAsset(
    symbol: string,
    price: number,
): Action {
    return {
        type: Actions.ADD_ASSET,
        symbol,
        price,
    };
}

export function addHolding(
    id: string,
    symbol: string,
    quantity: string,
    targetPct: string,
): Action {
    return {
        type: Actions.ADD_HOLDING,
        id,
        symbol,
        quantity: parseFloat(quantity) || 0,
        targetPct: parseFloat(targetPct) || 0,
    };
}

export function addPortfolio(): Action {
    return {
        type: Actions.ADD_PORTFOLIO,
    };
}

export function deleteAsset(symbol: string): Action {
    return {
        type: Actions.DELETE_ASSET,
        symbol,
    };
}

export function deleteHolding(id: string, symbol: string): Action {
    return {
        type: Actions.DELETE_HOLDING,
        id,
        symbol,
    };
}

export function deletePortfolio(id: string): Action {
    return {
        type: Actions.DELETE_PORTFOLIO,
        id,
    };
}

export function hideMessage(): Action {
    return {
        type: Actions.HIDE_MESSAGE,
    };
}

export function refreshAssets(): Action {
    return {
        type: Actions.REFRESH_ASSETS,
    };
}

export function renamePortfolio(id: string, name: string): Action {
    return {
        type: Actions.RENAME_PORTFOLIO,
        id,
        name,
    };
}

export function saveState(): Action {
    return {
        type: Actions.SAVE_STATE,
    };
}

export function setApiKey(apiKey: string): Action {
    return {
        type: Actions.SET_API_KEY,
        apiKey,
    };
}

export function showError(message: string): Action {
    return {
        type: Actions.SHOW_ERROR,
        message,
    };
}

export function updateAsset(symbol: string, price: number): Action {
    return {
        type: Actions.UPDATE_ASSET,
        symbol,
        price,
    };
}

export function updateHolding(
    id: string,
    symbol: string,
    quantity: string,
    targetPct: string,
): Action {
    return {
            type: Actions.UPDATE_HOLDING,
            id,
            symbol,
            quantity: +quantity,
            targetPct: +targetPct,
    };
}

export function updatePortfolios(): Action {
    return {
        type: Actions.UPDATE_PORTFOLIOS,
    };
}
