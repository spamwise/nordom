// Store state, and the loading/saving thereof.

import {
    Boolean,
    Dictionary,
    Literal,
    Number,
    Record,
    Static,
    String,
    Union,
} from "runtypes";

export const AssetRecord = Record({
    price: Number,      // price in dollars
    updated: Number,    // UTC timestamp of last API query
});

export const HoldingRecord = Record({
    quantity: Number,   // number of shares
    value: Number,      // value in dollars
    targetPct: Number,  // target percentage value of portfolio
    targetQty: Number,  // target number of shares (derived)
    actualPct: Number,  // actual percentage value of portfolio
});

export const MarketRecord = Record({
    apiKey: String, // API key for AlphaVantage
    assets: Dictionary(AssetRecord, "string"),
});

export const PortfolioRecord = Record({
    name: String,
    totalValue: Number,
    holdings: Dictionary(HoldingRecord, "string"),
});

export const UserMessageRecord = Record({
    msg: String,
    type: Union(Literal("error"), Literal("info")),
    visible: Boolean,
});

export const StateRecord = Record({
    market: MarketRecord,
    portfolios: Dictionary(PortfolioRecord, "string"),
    userMessage: UserMessageRecord,
});

export const theDollar = "$USD"; // the almighty buck

export type Asset       = Static<typeof AssetRecord>;
export type Holding     = Static<typeof HoldingRecord>;
export type Market      = Static<typeof MarketRecord>;
export type Portfolio   = Static<typeof PortfolioRecord>;
export type UserMessage = Static<typeof UserMessageRecord>;
export type State       = Static<typeof StateRecord>;

export const initialState: State = {
    market: {
        apiKey: "",
        assets: {
            [theDollar]: {price: 1, updated: 0},
        },
    },
    portfolios: {},
    userMessage: {msg: "", type: "info", visible: false},
};

export function loadState(): State {
    // Try to load state from local storage.
    // Returns initialState if it can't.
    const stateJson = localStorage.getItem("state");
    if (!stateJson) {
        // nothing saved, that's fine
        return initialState;
    }
    try {
        const state = JSON.parse(stateJson);
        return state;
    } catch (err) {
        console.log("couldn't parse state JSON: ", stateJson);
        console.log(err.message);
        return initialState;
    }
}

export function saveState(state: State) {
    localStorage.setItem("state", JSON.stringify(state));
}
