// Server calls that update our state.

export async function fetchPrice(symbol: string, apiKey: string): Promise<number> {
    const url = "https://www.alphavantage.co/query?" +
        `function=GLOBAL_QUOTE&symbol=${symbol}&apikey=${apiKey}`;
    const response = await fetch(url);
    const json = await response.json();
    const quote = json["Global Quote"];
    if (quote) {
        const price = +quote["05. price"];
        if (isNaN(price)) {
            console.error("Priceless quote:", quote);
            throw new Error("Error fetching price for " + symbol);
        }
        return price;
    }
    let error = "Error fetching price for " + symbol + ":\n";
    if (json["Error Message"]) {
        error += json["Error Message"];
    } else if (json.Information) {
        error += json.Information;
    } else if (json.Note) {
        error += json.Note;
    } else {
        console.error("Unknown AlphaVantage JSON response:", json);
    }
    throw new Error(error);
}
