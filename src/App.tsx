import React from "react";

import { updatePortfolios } from "./store/actions";
import reducer from "./store/reducer";
import rootSaga from "./store/sagas";
import { loadState } from "./store/state";
import Market from "./ui/market";
import Message from "./ui/message";
import Portfolios from "./ui/portfolios";
import Toolbar from "./ui/toolbar";

import { Provider } from "react-redux";
import { applyMiddleware, createStore } from "redux";
import createSagaMiddleware from "redux-saga";

const App: React.FC = () => {
  const sagaMiddleware = createSagaMiddleware();
  const store = createStore(
    reducer, 
    reducer(loadState(), updatePortfolios()), 
    applyMiddleware(sagaMiddleware),
  );
  sagaMiddleware.run(rootSaga);

  return (
    <Provider store={store}>
      <Market/>
      <Portfolios/>
      <Toolbar/>
      <Message/>
    </Provider>
  );
};

export default App;
