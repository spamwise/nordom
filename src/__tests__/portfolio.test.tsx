import {
    getByLabelText,
    getByPlaceholderText,
    getByRole,
    getByTestId,
    getByText,
    getByTitle,
    queryByTitle,
    getByDisplayValue,
} from "@testing-library/dom";
import { fireEvent, render, RenderResult } from "@testing-library/react";
import React from "react";

import App from "../App";
import { initMarketState } from "./market.test";

async function addHolding(
    app: RenderResult,
    portfolio: HTMLElement,
    symbol: string,
    quantity: number,
    target: number) {
        fireEvent.click(getByTitle(portfolio, "Add Asset"));
        const select = await app.findByTestId("portfolio-select");
        fireEvent.click(getByRole(select, "button"));
        const list = app.getByRole("listbox");
        fireEvent.click(getByText(list, symbol));
        fireEvent.change(
            app.getByPlaceholderText("Quantity"),
            {target: {value: quantity}},
        );
        fireEvent.change(
            app.getByPlaceholderText("Target %"),
            {target: {value: target}},
        );
        fireEvent.click(getByTitle(portfolio, "Save"));
}

describe("<Portfolio />", () => {
    it("can add and delete portfolios", async () => {
        // Load app with 3 market assets.
        localStorage.setItem("state", JSON.stringify(initMarketState()));
        const app = render(<App/>);
        localStorage.clear();

        // Add a new portfolio.
        fireEvent.click(app.getByText("New Portfolio"));

        // Add all three assets to it.
        let portfolio = app.getByTestId("portfolio-Portfolio 1");
        await addHolding(app, portfolio, "ESGE", 100, 10);
        portfolio = app.getByTestId("portfolio-Portfolio 1");
        await addHolding(app, portfolio, "ESML", 10, 80);
        portfolio = app.getByTestId("portfolio-Portfolio 1");
        await addHolding(app, portfolio, "SPYX", 20, 10);

        // The "Add Asset" action should no longer be available.
        portfolio = app.getByTestId("portfolio-Portfolio 1");
        expect(queryByTitle(portfolio, "Add Asset") === null);

        // Add more ESML so the balancer tells us to sell it off.
        portfolio = app.getByTestId("portfolio-Portfolio 1");
        let row = getByTestId(portfolio, "portfolio-row-ESML");
        fireEvent.click(getByTitle(row, "Edit"));
        let editRow = getByTestId(portfolio, "portfolio-edit-row");
        fireEvent.change(
            getByPlaceholderText(editRow, "Quantity"),
            { target: { value: "20" } },
        );
        fireEvent.click(getByTitle(editRow, "Save"));

        // Add cash and get the balancer to tell us to spend it.
        portfolio = app.getByTestId("portfolio-Portfolio 1");
        row = getByTestId(portfolio, "portfolio-row-$USD");
        fireEvent.click(getByTitle(row, "Edit"));
        editRow = getByTestId(portfolio, "portfolio-edit-row");
        fireEvent.change(
            getByPlaceholderText(editRow, "Quantity"),
            { target: { value: "100" } },
        );
        fireEvent.change(
            getByPlaceholderText(editRow, "Target %"),
            { target: { value: "0" } },
        );
        fireEvent.click(getByTitle(editRow, "Save"));

        // Delete the ESGE holding from the portfolio.
        portfolio = app.getByTestId("portfolio-Portfolio 1");
        row = getByTestId(portfolio, "portfolio-row-ESGE");
        fireEvent.click(getByTitle(row, "Delete"));
        fireEvent.click(app.getByTitle("Save"));

        // Rename the portfolio.
        portfolio = app.getByTestId("portfolio-Portfolio 1");
        fireEvent.click(getByTitle(portfolio, "Rename portfolio"));
        expect(queryByTitle(portfolio, "Rename portfolio") === null);
        fireEvent.change(
            getByDisplayValue(portfolio, "Portfolio 1"),
            { target: { value: "asdf"} },
        );
        fireEvent.click(getByTitle(portfolio, "OK"));
        portfolio = app.getByTestId("portfolio-asdf");
        getByTitle(portfolio, "Rename portfolio");

        // Delete the entire portfolio.
        portfolio = app.getByTestId("portfolio-asdf");
        fireEvent.click(getByTitle(portfolio, "Delete portfolio"));
        fireEvent.click(getByText(app.getByTestId("delete-dialog"), "Delete"));
        expect(app.queryByTestId("portfolio-asdf") === null);
    });
});
