import assert from "assert";
import {Actions} from "../store/actions";
import reduce from "../store/reducer";
import {State} from "../store/state";

describe("Reducer tests:", function() {
    it("Portfolio balances correctly", function() {
        const oldState: State = {
            market: {
                apiKey: "",
                assets: {
                    $USD: {price: 1.0, updated: 0},
                    SPYX: {price: 10.0, updated: 0},
                    ESGE: {price: 20.0, updated: 0},
                    CMF: {price: 5.0, updated: 0},
                    TAN: {price: 10.0, updated: 0},
                },
            },
            portfolios: {
                [1]: {
                    name: "Taxable",
                    totalValue: 0,
                    holdings: {
                        SPYX: {
                            quantity: 40,
                            targetPct: 40,
                            targetQty: 0,
                            value: 0,
                            actualPct: 0,
                        },
                        ESGE: {
                            quantity: 15,
                            targetPct: 30,
                            targetQty: 0,
                            value: 0,
                            actualPct: 0,
                        },
                        CMF: {
                            quantity: 40,
                            targetPct: 20,
                            targetQty: 0,
                            value: 0,
                            actualPct: 0,
                        },
                        TAN: {
                            quantity: 10,
                            targetPct: 10,
                            targetQty: 0,
                            value: 0,
                            actualPct: 0,
                        },
                        $USD: {
                            quantity: 500,
                            targetPct: 0,
                            targetQty: 0,
                            value: 0,
                            actualPct: 0,
                        },
                    },
                },
            },
            userMessage: {msg: "", type: "info", visible: false},
        };
        const newState = reduce(oldState, {type: Actions.UPDATE_PORTFOLIOS});
        const portfolio = newState.portfolios[1];

        assert(portfolio.totalValue == 1500);
        assert(portfolio.holdings.SPYX.value == 400);
        assert(portfolio.holdings.SPYX.targetQty == 60);
        assert(portfolio.holdings.SPYX.actualPct == 400 / 15);
        assert(portfolio.holdings.$USD.value == 500);
        assert(portfolio.holdings.$USD.targetQty == 0);
    });
});
