import {
    getByLabelText,
    getByPlaceholderText,
    getByRole,
    getByText,
    getByTitle,
} from "@testing-library/dom";
import { fireEvent, render, RenderResult } from "@testing-library/react";
import React from "react";

import App from "../App";
import { initialState, State } from "../store/state";

const fetchMock = require("fetch-mock");

async function addAsset(app: RenderResult, symbol: string, price: number) {
    const { getByPlaceholderText, getByTitle } = app;
    const addButton = await getByTitle("Add Asset");
    fireEvent.click(addButton);
    const symbolField = await getByPlaceholderText("Symbol");
    const priceField = await getByPlaceholderText("Price");
    fireEvent.change(symbolField, {target: {value: symbol}});
    fireEvent.change(priceField, {target: {value: price}});
    const saveButton = await getByTitle("Save");
    fireEvent.click(saveButton);
}

export function initMarketState(): State {
    return {
        ...initialState,
        market: {
            ...initialState.market,
            assets: {
                ...initialState.market.assets,
                ["SPYX"]: {
                    price: 101,
                    updated: Date.now() - 1000 * 60 * 60 * 24 * 3,
                },
                ["ESGE"]: {
                    price: 110,
                    updated: Date.now() - 1000 * 60 * 60 * 3,
                },
                ["ESML"]: {
                    price: 111,
                    updated: Date.now() - 1000 * 60 * 5 * 3,
                },
            },
        },
    };
}

describe("<Market />", () => {
  it("can add, edit, and delete assets", async () => {
    const app = render(<App/>);
    await addAsset(app, "SPY", 100.0);

    // Row should be in the table now with a price of $100.
    let row = app.getByTestId("market-row-SPY");
    getByText(row, "SPY");
    getByText(row, "$100.00");

    // Can't add it again though!
    await addAsset(app, "SPY", 120.0);
    const message = app.getByTestId("message-snackbar");
    fireEvent.click(getByLabelText(message, "close"));
    expect(app.queryByTestId("message-snackbar") === null);

    // Click the edit button and change price via the edit row.
    row = app.getByTestId("market-row-SPY");
    const editButton = getByTitle(row, "Edit");
    fireEvent.click(editButton);
    row = await app.findByTestId("market-edit-row");
    const priceField = getByPlaceholderText(row, "Price");
    fireEvent.change(priceField, {
        target: { value: "123" },
    });
    fireEvent.click(getByTitle(row, "Save"));

    // Row should re-render with the new price.
    row = app.getByTestId("market-row-SPY");
    getByText(row, "$123.00");

    const deleteButton = getByTitle(row, "Delete");
    fireEvent.click(deleteButton);
    fireEvent.click(app.getByTitle("Save"));

    expect(app.queryByTestId("market-row-SPY") === null);
  });

  it("loads assets from localstorage", async () => {
    localStorage.setItem("state", JSON.stringify(initMarketState()));

    const app = render(<App/>);
    localStorage.clear();

    await app.findByTestId("market-row-SPYX");
    await app.findByTestId("market-row-ESGE");
    await app.findByTestId("market-row-ESML");
  });

  it("can load quotes from the server", async () => {
    const app = render(<App/>);
    fetchMock.mock("begin:https://www.alphavantage.co/", {
        ["Global Quote"]: {
          ["05. price"]: "150.0",
        },
      });
    await addAsset(app, "SPY", 100.0);
    const APIbutton = await app.getByText("Set API Key");
    fireEvent.click(APIbutton);
    const APIfield = await app.getByTestId("api-field");
    fireEvent.change(getByRole(APIfield, "textbox"), {
        target: { value: "asdf "},
    });
    fireEvent.click(app.getByText("Update Key"));

    jest.spyOn(Date, "now").mockImplementation(() => 1479427200000);
    const refreshButton = await app.getByTitle("Load Asset Prices");
    fireEvent.click(refreshButton);

    await app.findByText("$150.00");
  });
});
