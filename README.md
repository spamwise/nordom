# Portfolio balancing calculator

This is a tool to help you figure out which trades to make to bring an
investment portfolio into alignment with a specific asset mix.  It won't
tell you how to set up your asset mix, just how many shares of each asset
you need to buy or sell to balance it out.

Current version deployed to http://nordom.leadtogold.com.

## How do you use it?

1. Add assets to the "Market Assets" table at the top of the page.
2. Add an API key to enable quick fetching of asset values from AlphaVantage.
3. Click the "Load" button in the market table to request fresh data.
4. Click "New Portfolio" at the bottom of the page to add a new portfolio.
5. Add assets to the portfolio.  You can specify the quantity you
   currently hold of each, and the target percentage allocation of each.

As you add and update assets in your portfolio, the *Value* will be
automatically calculated based on the *Price* in the market, and a *Trade*
will be suggested in order to bring the *Value* in line with the *Target %*.

## Saving your place

The *Save Data* button will save all of your information to your browser's
*local state* (not to the cloud).  When you refresh the page, the last saved
local state will be loaded.